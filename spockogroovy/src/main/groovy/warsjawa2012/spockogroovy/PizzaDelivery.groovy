package warsjawa2012.spockogroovy

class PizzaDelivery {

    Integer quantity
    String topping
    Date time

    PizzaDelivery need(Integer quantity){
        this.quantity = quantity
        return this
    }

    PizzaDelivery with(String topping){
        this.topping = topping
        return this
    }

    PizzaDelivery at(String time) {
        this.time = Date.parse('h:mm', time)
        return this
    }

    String toString(){
        return "${quantity} ${topping} at ${time.format('h:mm')} hours"
    }
}
package warsjawa2012.spockogroovy

class Greeter {
    String greet(String personToGreet) {
        'Hello, ' + personToGreet + '!'
    }
}

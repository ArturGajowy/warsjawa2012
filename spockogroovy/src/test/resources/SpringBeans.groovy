import warsjawa2012.spockogroovy.Person

// ref: http://grails.org/doc/latest/guide/spring.html

beans {

    aString(String, 'Spring bean from Groovy')

    // bean definition can be changed based on configuration or other code can be executed
    // it's really usable for automation tests to instantiate mock services, DAOs, etc...
    if (System.currentTimeMillis() % 2) {
        aNumber(Integer, 3 + 5)
    } else {
        aNumber(Double, 25.15)
    }

    // hint: use ref('beanName') to inject dependency
}
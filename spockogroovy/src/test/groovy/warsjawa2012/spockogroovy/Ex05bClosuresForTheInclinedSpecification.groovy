package warsjawa2012.spockogroovy

import spock.lang.Specification
import spock.lang.Unroll
import spock.lang.Ignore

class Ex05bClosuresForTheInclinedSpecification extends Specification {


    /*
    Exercise:
     write an implementation of fixedPoint operator (a higher-order function that takes a value (point 'x'),
     a function 'f', and repeatedly computes x' = f(x) until x' == x (meaning that f(x') = x',
     meaning that x' is a fixed point of f) and returns x'.

    Exercise:
     write a closure 'flattenOneLevel' such that for every list containing only scalars or other lists:
       fixedPoint(list, flattenOneLevel) == list.flatten()
    */
    @Ignore('exercise')
    @Unroll('fixedPoint statement implementation: #implementation')
    def 'fixedPoint control flow statement'() {
        given:
        def desiredLength = 3
        def enlargeUntilDesiredLength = { list -> list.size() == desiredLength ? list : list + [1] }

        expect:
        this."$implementation"([1], enlargeUntilDesiredLength) == [1] * desiredLength

        and:
        fixedPoint([1, [2, [3, [4]]], 1], flattenOneLevel) == [1, 2, 3, 4, 1]

        where:
        implementation << ['fixedPoint', 'fixedPointRecursive', 'fixedPointTrampolined']
    }

    def fixedPoint(value, transformation) {
        'exercise :)'
    }

    def fixedPointRecursive(value, transformation) {
        'exercise :)'
    }

    def fixedPointTrampolined(value, transformation) {
        'exercise :)'
    }

    def flattenOneLevel = { list ->
        'exercise :)'
    }

    //TODO Exercise: write a method that will try to evaluate a condition (parameter) to true at most 5 times,
    //then give up and return false


    /*
    Want more? YEAH! :D
     - http://mrhaki.blogspot.com/2011/04/groovy-goodness-chain-closures-together.html
     - http://docs.codehaus.org/display/GROOVY/Groovy+1.8+release+notes#Groovy18releasenotes-Closurefunctionalflavors
    */
}

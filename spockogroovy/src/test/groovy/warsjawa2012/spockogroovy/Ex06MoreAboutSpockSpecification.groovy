package warsjawa2012.spockogroovy

import spock.lang.Specification

// ref: http://code.google.com/p/spock/wiki/Interactions
class Ex06MoreAboutSpockSpecification extends Specification {

    PersonService personService = new PersonService()
    PersonDao personDaoMock = Mock(PersonDao)

    def setup() {
        personService.dao = personDaoMock
    }




    def 'expect the exception'() {

        when:
        personService.get(0)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'id must by greater than zero'
    }




    def 'interaction with specified parameter'() {

        when:
        personService.getAllAdults()

        then:
        1 * personDaoMock.findAllOlderThan(18)
    }




    def 'interaction with any parameter check'() {

        when:
        personService.get(5)
        personService.get(10)
        personService.get(15)

        then:
        3 * personDaoMock.findById(_)
    }




    def 'no interaction check'() {

        given:
        Person person = new Person('Matt', 27)

        when:
        personService.save(person)

        then:
        0 * personDaoMock.update(_)
        1 * personDaoMock.create('Matt', 27)
    }




    def 'return something from a mock'() {

        given:
        Person person = new Person('Matt', 27)
        person.id = 523
        personDaoMock.findById(523) >> person

        when:
        personService.save(person)

        then:
        1 * personDaoMock.update(person)
    }




    def 'return something from a mock and check interaction'() {

        given:
        Person person = new Person('Jane', 21)
        person.id = 415

        when:
        personService.save(person)

        then:
        1 * personDaoMock.findById(415) >> person
    }
}

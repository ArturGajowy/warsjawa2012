package warsjawa2012.spockogroovy

import spock.lang.Specification
import org.codehaus.groovy.runtime.typehandling.GroovyCastException
import spock.lang.Ignore

class Ex01SomeGroovynessSpecification extends Specification {

    def "dynamic typing - 'def' basically means 'Object'"() {

        when:
        def dynamic  =  1
        dynamic = "I am a String stored in a variable of dynamic type"

        then:
        noExceptionThrown()

        when:
        int typed = 2
        typed = "I am a String stored in a variable of type int??"

        then:
        def exception = thrown(ClassCastException)
        exception instanceof GroovyCastException
        exception.message.contains('Cannot cast object')
    }




    /*
      - '==' is a bit better than equals:
      - it's null-proof (on both sides :))
      - it uses built-in Groovy coercions
    */
    def "'==' means 'equals', '!=' means '!equals', 'is' means '=='"() {

        given:
        def list = [1, 2, 3]
        def listWithSameContents = [1, 2, 3]

        expect:
        list == listWithSameContents
        list != []
        list.is(list)
        !list.is(listWithSameContents)
    }




    def "'==' is better than equals"() {

        given:
        def parityOfOne = "${getParity(1)}"
        def expectedParity = 'not even!'

        expect:
        parityOfOne == expectedParity
        expectedParity instanceof String
        parityOfOne instanceof GString
        !parityOfOne.equals(expectedParity)
    }




    def 'return statements are optional'() {

        expect:
        getParity(0) == 'even!'
        getParity(1) == 'not even!'
    }

    def getParity(int i) {
        if (i % 2 == 0) {
            'even!'
        } else {
            'not even!'
        }
    }




    class Car {

        int speed
        int altitude
    }

    def 'getters and setters are optional'() {

        given:
        def car = new Car()

        when:
        car.speed = 500

        then:
        car.speed == 500

    }

    @Ignore('exercise: change the Car class so it throws IllegalArgumentException on altitude get')
    def 'ensure getters are called'() {

        given:
        def car = new Car()

        when:
        car.altitude

        then:
        def ex = thrown(IllegalStateException)
        ex.message == "cars don't fly!"
    }

    @Ignore('exercise: change the Car class so it throws IllegalArgumentException on altitude set')
    def 'ensure setters are called'() {

        given:
        def car = new Car()

        when:
        car.altitude = 100

        then:
        def ex = thrown(IllegalStateException)
        ex.message == "cars don't fly!"
    }




    def 'methods can be called without parentheses and even dots (Groovy sentences)'() {

        given:
        PizzaDelivery pizzaDelivery = new PizzaDelivery()

        when:
        pizzaDelivery.with {
            need 3 with 'pepperoni' at '6:00' // need(3).with('pepperoni').at('6:00')
        }

        then:
        pizzaDelivery as String == '3 pepperoni at 6:00 hours'
    }




    @Ignore("exercise: use 'elvis operator' at 'when' block")
    def 'Elvis operator'() {

        given:
        Person person = new Person()

        when:
        String name = person.name // hint: Elvis operator is ?:

        then:
        name == 'name unknown'
    }




    @Ignore("exercise: adjust 'when' block without using any loop nor closure")
    def 'spread operator'() {

        given:
        def strings = ['abcd', 'efg', 'hijkl']

        when:
        def uppercaseStrings // hint: String object has toUpperCase() method

        then:
        uppercaseStrings == ['ABCD', 'EFG', 'HIJKL']
    }




    @Ignore("exercise: correct 'when' block with a single question mark")
    def 'safe reference operator'() {

        given:
        Person person = null

        when:
        String name = person.name

        then:
        noExceptionThrown()
    }




    class ListJoiner {
        String join(List aList, String separator) {
            aList.join(separator)
        }
    }

    @Ignore('exercise: adapt ListJoiner.join() method with optional parameter')
    def 'optional parameters'() {
        
        given:
        def joiner = new ListJoiner()
        def numbers = [4, 5, 6, 7, 8]
        
        when:
        def joined = joiner.join(numbers)

        then:
        joined == '4|5|6|7|8'
    }




    def 'Groovy tries to protect you from float arithmetic nuances'() {

        given:
        0.1 instanceof BigDecimal
    }




    def 'Groovy makes working with huge numbers easier :)'() {

        given:
        def trillionOrQuintillion = 10_000_000_000_000_000_000

        expect:
        trillionOrQuintillion > Long.MAX_VALUE
        trillionOrQuintillion instanceof BigInteger
    }




    def 'Groovy, however, does not protect from integer overflow :)'() {

        expect:
        Integer.MAX_VALUE + 1 == Integer.MIN_VALUE
    }




    def "Groovy by default ignores Java's access modifiers" () {
        expect:
        new Person('Johny', 42).privates == Person.PRIVATES_VALUE
    }


}

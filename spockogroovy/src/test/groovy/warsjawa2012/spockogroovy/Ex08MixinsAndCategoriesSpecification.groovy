package warsjawa2012.spockogroovy

import spock.lang.Specification
import spock.lang.Unroll
import groovy.time.TimeCategory
import spock.lang.Ignore

class Ex08MixinsAndCategoriesSpecification extends Specification {

    @Unroll('using PokemonishCategories.#pokemonishCategory.getSimpleName()')
    def 'use Pokemonish categories'() {

        when:
        'Hi there!'.pokemonish()

        then:
        thrown(MissingMethodException)

        when:
        use(pokemonishCategory) {
            'Hi there!'.pokemonish() == 'Hi tHeRe!'
            'Hi there!'.pokemonish(3) == 'Hi TheRe!'
        }

        then:
        noExceptionThrown()

        when:
        'Hi there!'.pokemonish()

        then:
        thrown(MissingMethodException)

        where:
        pokemonishCategory << [PokemonishCategories.ByHand, PokemonishCategories.Generated]
    }

    def 'use Vehicle mixins'() {
        expect:
        new Plane().fly() == "I'm the Concorde and I fly!"
        new Submarine().dive() == "I'm the Yellow Submarine and I dive!"

        new JamesBondVehicle().fly() == "I'm the James Bond's vehicle and I fly!"
        new JamesBondVehicle().dive() == "I'm the James Bond's vehicle and I dive!"
    }

    class AdHocVehicle implements Vehicle {
        String name = 'ad hoc vehicle'
    }

    def 'runtime mixins'() {
        when:
        def adHocVehicle = new AdHocVehicle()
        def anotherAdHocVehicle = new AdHocVehicle()
        adHocVehicle.metaClass.mixin(FlyingAbility)

        then:
        adHocVehicle.fly() == "I'm the ad hoc vehicle and I fly!"

        when:
        anotherAdHocVehicle.fly()

        then:
        thrown(MissingMethodException)

        when:
        new AdHocVehicle().fly()

        then:
        thrown(MissingMethodException)
    }

    /*
    Never, never ever do this!
     */
    @Ignore //this test could break the previous ones - changes in classes are somewhat global... ;)
    def 'runtime, class global mixins' () {
        when:
        def legacyAdHocVehicle = new AdHocVehicle()
        AdHocVehicle.mixin(FlyingAbility)
        def adHocVehicle = new AdHocVehicle()

        then:
        adHocVehicle.fly() == "I'm the ad hoc vehicle and I fly!"

        when:
        legacyAdHocVehicle.fly()

        then:
        thrown(MissingMethodException)
    }

    def 'TimeCategory is usable for dates manipulation'() {

        given:
        Date currentDate = new Date()
        Date aWeekAgo

        when:
        use(TimeCategory) {
            aWeekAgo = 1.week.ago
        }

        then:
        currentDate > aWeekAgo
    }



    @Ignore("exercise: adjust date calculation at 'when' block")
    def 'TimeCategory is really usable!'() {

        given:
        String format = 'yyyy-MM-dd HH:mm'
        Date date = Date.parse(format, '2012-05-15 12:00')

        when:
        use(TimeCategory) {
            date += 1 // hint: check what properties you've got here on numbers (ctrl+space)
        }
        then:
        date == Date.parse(format, '2015-07-16 20:15')
    }
}

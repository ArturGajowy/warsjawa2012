package warsjawa2012.spockogroovy

import spock.lang.Specification
import spock.lang.Ignore

class Ex04bListsAndRangesSpecification extends Specification {

    def longList = [1, 2, 3, 4, 5]

    def 'convenient list elements access'() {

        given:
        def aSensibleValue = 42
        def middleIndex = 2

        expect: 'first and last methods'
        longList.first() == 1
        longList.last() == 5

        and: 'array subscript operator'
        longList[middleIndex] == 3

        when: 'assigning a value using subsrcipt operator'
        longList[middleIndex] = aSensibleValue

        then: 'everything works as expected ;)'
        longList[middleIndex] == aSensibleValue
    }

    def 'list transposition'() {
        expect:
        [
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9],
                [A, B, C]
        ].transpose() == [
                [1, 4, 7, A],
                [2, 5, 8, B],
                [3, 6, 9, C]
        ]

        where:
        A = 'A'
        B = 'B'
        C = 'C'
    }

    def 'ranges are lists'() {

        given:
        def range = 1 .. 5
        def halfOpenRange = 1 ..< 6

        expect:
        range == longList
        halfOpenRange == longList

        and:
        range instanceof java.util.List
        halfOpenRange instanceof java.util.List

        and:
        5 .. 1 == longList.reverse()
        'a' .. 'e' == 'abcde' as List
    }

    def 'use ranges for easy iteration'() {
        given:
        def results = []

        when:
        (0..10).step(3) { i ->
            results << i
        }

        then:
        results == [0, 3, 6, 9]
    }

    def 'kick-ass range definitions'() {
        expect:
        [1, *3..5, 8, *10..<12] == [1, 3, 4, 5, 8, 10, 11]
    }

    def 'kick-ass subscript operator features'() {

        expect: 'it accepts negative indices for backwards indexing'
        longList[-1] == longList.last()
        longList[-3] == 3
        longList[-longList.size()] == longList.first()

        and: 'it accepts multiple argumnents'
        longList[4, 2, 3] == [5, 3, 4]

        and: 'it accepts range arguments'
        longList[1 .. 3] == [2, 3, 4]

        and: 'their powers combine'
        longList[1 .. -2] == longList[1 .. 3]
        longList[-2 .. 1] == longList[1 .. -2].reverse()

        longList[4 .. 0] == longList.reverse()
        longList[-1 .. -5] == longList[4 .. 0]
        longList[-5 .. -1] == longList[-1 .. -5].reverse()
    }

    def 'default values for inexistent entries'() {
        given:
        def basicallySquares = [:].withDefault { key -> key ** 2 }

        expect:
        basicallySquares[2] == 4
        basicallySquares[4] == 16

        when:
        basicallySquares[2] = 'Kaboom!'

        then:
        basicallySquares[2] == 'Kaboom!'
        basicallySquares[4] == 16
    }

    /*
    Exercise:
     count the occurences of words in the follwing string. Try not using if statements or any syntax containing '?' :)
    */
    @Ignore('exercise')
    def 'counting occurences of interesting words in a text'() {
        given:
        def text = 'Mary had a little lamb la la la la la la la'
        def interestingWords = ['Mary', 'cat', 'la']

        when:
        //HINT: should you need a method you think we did not mention yet - ask :) We'll not spoil the fun anyway ;)
        def interestingOccurrences = 'exercise :)'


        then:
        interestingOccurrences == [Mary: 1, cat: 0, la: 7]
    }

    /*
    Side note:
     Liss also have a withDefault method - in two flavors (withLazyDefault and withEagerDefault)
     Check out the docs!
    */
}

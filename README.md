
# Please do this before the workshop

## 0. Stay tuned!

Visit this page after Friday, 20.00 and update your setup by doing

    $ git pull

and re-executing steps 3. and 4. of this guide (this should go really quick).

Most probably, the Internet connection we'll be given cannot be relied upon **at all**. This is why it's super-important that you do this all **before** the workshop.

As soon as you can, do all the steps listed below:

## 1. Clone this GIT repo

Click the clone button above and execute the command shown there in shell.

If you don't have git installed, download and install it from [here](http://git-scm.com/download)

## 2. Install Gradle
     
 - unzip [Gradle binary distribution](http://services.gradle.org/distributions/gradle-1.2-bin.zip) to a path of your choice
 - update system variables:
 
  1. GRADLE\_HOME="path/to/gradle-1.2" (use a file path separator according to your OS, don't add a trailing slash)
  2. add GRADLE\_HOME/bin to path:
     - on linux: PATH=$PATH:$GRADLE\_HOME/bin
     - on windows: add ';%GRADLE\_HOME%\bin' to your Path variable
  3. you'll also need JAVA_HOME set to your JDK's installation folder

## 3. Build the project

 - in project root dir (the 'spockogroovy' dir) do:

        $ gradle build

Some dependencies should be downloaded and you should see something like this:

    :compileJava
    :compileGroovy
    :processResources UP-TO-DATE
    :classes
    :jar
    :assemble
    :compileTestJava UP-TO-DATE
    :compileTestGroovy
    :processTestResources UP-TO-DATE
    :testClasses
    :test
    :check
    :build
    
    BUILD SUCCESSFUL
    
    Total time: 7.615 secs

Make sure that there was 'BUILD SUCCESSFUL' output in the console :)

## 4. Generate Intellij IDEA project (just in case, do this even if you won't use IDEA)

In project root dir do:

    $ gradle idea


## 5. Install Intellij IDEA (optional but recommended)

This will generate IDE project files (You can try to add plugin eclipse/netbeans to build.gradle file and try similiar with IDE of your choice. We haven't tested it though).

Install [IntelliJ IDEA](http://www.jetbrains.com/idea/download/) (community edition will be enough).

Run it.

File > Open project... Navigate to repo dir and open the project file (.ipr)

If IDE pops up with Unregistered Git root detected, click Configure then Add root and OK. You should have git command available from PATH if you want to enable GIT integration.

## 6. Build project with Gradle under IDE(A):

 - open Project tree on the left
 - right-click gradle.build file, select Run 'build'.
   
It will run Gradle but without task parameter. It's not good enough yet so...
   
 - At the menu bar there is dropdown menu with green 'G' logo now. Click it and select Edit Configurations...
 - set Script parameters to: build, and click OK (you can also click save icon before, to make this configuration pernament for the project).
   
Now after clicking play button you should see full Gradle build (as shown above)

## 7. Run tests under IDE(A) with nice reporting

> Warning: This step will fail if there's no project SDK defined yet. To fix it, click File > Project Structure... adn select a Java 1.7 SDK (creating it if necessary). To create a Java SDK: click New and point to JAVA 1.7 JDK installation path (JDK, not JRE). Leave the default SDK name '1.7' as build.gradle referes to that.

Unfold at project tree to src/test and right-click on groovy folder. Select Run 'All Tests'. All tests will be run :) One is false negative as 'Failed to start' (yellow light). It's because of Spock's @Unroll annotation. Don't worry it's fine.

Passed tests are hidden by default. To see them uncheck Hide passed icon, which is the 2nd icon in tests menu bar with a funnel on it (pl. lejek).
